# Repository

This repository contains the source code to calculate *skipped Exons* from a given *gtf*. There are two versions. Version 1.0 iterates all *CDSs* from a *gene* and calculate its *introns*, then it checks *CDS* by *CDS* if it is a *skipped Exons*. This solution is in the worst case `O(n^2)`  where `n` is the number of *CDSs* per *gene*, implementation can be seen [here](https://gitlab.com/nesterran/exon-skipt-calculator/blob/master/src/main/java/com/nestor/exskip/ExonSkipCalculatorV1.java). Version 2.0 differs from V1.0 in that it groups *introns* by their *start* and *end* distance in a list, and at the same time creates a *linked-list* between introns of the same *transcript*, then iterates the list of gruped introns to match which *introns* are inside other *introns*, this has the same effect as checking for *CDSs* inside *introns*. The overall performance of the algorithm is ´O(n)´ where `n` is the number of *introns* in the given *gene*. Implementation can be seen here [here](https://gitlab.com/nesterran/exon-skipt-calculator/blob/master/src/main/java/com/nestor/exskip/ExonSkipCalculator.java).

The process to read *genes* from a *gtf* file, takes streams from disk to memory *gene* by *gene* to avoids loading the whole file in memory. Implementation can be seen [here](https://gitlab.com/nesterran/exon-skipt-calculator/blob/master/src/main/java/com/nestor/exskip/GenStreamReader.java).

The following Figure shows the use cases contemplated by the algorithm to find *skipped Exons*:
![alt text](analisys_skipped_exons.png "Contemplated Use Cases")

# Prerequisites

- Java 1.8
- Maven 3.3.9

# Build

Please run `mvn -B verify`. This task will build a *jar* file to `target/exskip-x.x.x.jar` and will run the unit tests.

# Test

Please run `mvn -B test`

# Run

Please run `mvn exec:java`. 

Command line arguments can be set in ´pom.xml´ file [here](https://gitlab.com/nesterran/exon-skipt-calculator/blob/master/pom.xml#L38).

Command line arguments:

- `-gtf INPUT_FILE_NAME` (required)
- `-o OUTPUT_FILE_NAME` (required)
- `-v VERSION`: Algorithm version, that is `1` or `2`. Default `2`
- `-p ACTIVATE`: Load *genes* in parallel. Allowed values are `0` or `1`. By default it is activated, that is `1`

