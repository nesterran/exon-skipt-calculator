package com.nestor.exskip;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import com.nestor.exskip.model.*;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * ExonSkiptCalculatorTest
 */
public class ExonSkipCalculatorTest  {
    
    @Test
    public void testCalculatorSimpleClase(){
        // Arrange
        
        GeneReading geneReading = new GeneReading(){{
            geneID="a gen id";
            codingRegions = Arrays.asList(
                new Transcript("CDS_0", Arrays.asList(new Range(0,167), new Range(6681,6821), new Range(7909,8103))),
                new Transcript("CDS_1", Arrays.asList(new Range(0,167), new Range(6681,6821), new Range(7909,8103))),
                new Transcript("CDS_2", Arrays.asList(new Range(0,167), new Range(6681,6821), new Range(7909,8103))),
                new Transcript("CDS_3", Arrays.asList(new Range(0,167), new Range(6681,6690), new Range(6812,6821), new Range(7909,8103))),
                new Transcript("CDS_4", Arrays.asList(new Range(0,167), new Range(6681,6821), new Range(7909,8103))),
                new Transcript("CDS_5", Arrays.asList(new Range(0,167), new Range(6681,6821), new Range(7909,8103))),
                new Transcript("CDS_6", Arrays.asList(new Range(0,167), new Range(7909,8115))),
                new Transcript("CDS_7", Arrays.asList(new Range(0,167), new Range(7909,8103))),
                new Transcript("CDS_8", Arrays.asList(new Range(6681,6821)))
            );
        }};

        SkipInfo si = new SkipInfo(){{
            geneID= "a gen id";
            numberTranscripts = 9;
            strand = "-";
            minSkippedExon =1;
            minSkippedBases = 20;
            maxSkippedExon = 2;
            maxSkippedBases=141;

            SVProps = new HashSet<String>(Arrays.asList("CDS_6", "CDS_7"));
            WTProps = Arrays.asList("CDS_0", "CDS_1","CDS_2", "CDS_3","CDS_4", "CDS_5"); 
            SVIntron = new Range(168, 7909);
            WTIntrons = new HashSet<Range>(Arrays.asList(
                new Range(168,6681), 
                new Range(6691,6812), 
                new Range(6822,7909)));
        }};
        SkipInfo[] expected = new SkipInfo[]{si};
        
        // Act
        for (IExonSkipCalculator calculator : Arrays.asList(new ExonSkipCalculator(), new ExonSkipCalculatorV1())) {
            SkipInfo[] actual = calculator.Calculate(geneReading);
        
            // Assert
            for (SkipInfo siInfo : expected) {
                Boolean found = false;
                for (SkipInfo siFind : actual) {
                    if (siInfo.equals(siFind)){
                        found = true;
                        break;
                    }
                }
                assertTrue(found);
            }

            for (SkipInfo siInfo : actual) {
                Boolean found = false;
                for (SkipInfo siFind : expected) {
                    if (siInfo.equals(siFind)){
                        found = true;
                        break;
                    }
                }
                assertTrue(found);
            }
        }
        
    }

}