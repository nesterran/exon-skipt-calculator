package com.nestor.exskip;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.nestor.exskip.model.*;
/**
 * ExonSkipCalculator
 */
 public class ExonSkipCalculator implements IExonSkipCalculator{
    
    public SkipInfo[] Calculate(GeneReading gene)  {
        Map<Long, Node> intronPoints = getIntronPoints(gene);
        Map<Range, SkipInfo> skiptInfosMap = new HashMap<Range,SkipInfo>();        
        Set<Long> alreadyProcessed = new HashSet<Long>();
        for (Map.Entry<Long, Node> kv : intronPoints.entrySet()) {
            Node point = kv.getValue();            
            evaluatePoint(point, alreadyProcessed, skiptInfosMap, intronPoints, gene);            
        }

        int size = skiptInfosMap.values().size();
        return skiptInfosMap.values().toArray(new SkipInfo[size]);
    }

    private void evaluatePoint(Node point, Set<Long> alreadyProcessed, Map<Range, SkipInfo> skiptInfosMap, Map<Long, Node> intronPoints, GeneReading gene){
        // If there are no more than two intros in the same start distance
        // that point has no skipped exons
        if (point.start.size() <= 1) {
            return;
        }

        if (alreadyProcessed.contains(point.location)){
             return;
        }
        
        alreadyProcessed.add(point.location);        
        
        for (Map.Entry<String, Intron> start : point.start.entrySet()) {
            String proteinID = start.getKey();
            Intron intron = start.getValue();
            Range svIntronRange = intron.location;
            
            
            if (skiptInfosMap.containsKey(svIntronRange)) {
                skiptInfosMap.get(svIntronRange).SVProps.add(intron.proteinID);
                continue;
            }
        
            // There are no matches to find ekiped exons
            if (intronPoints.get(svIntronRange.end).end.size() <= 1) {
                continue;
            }
            
            for(Map.Entry<String, Intron> end : intronPoints.get(svIntronRange.end).end.entrySet()) {
                String endProteinID = end.getKey();
                Intron endInnerIntron = end.getValue();
                
                // Skipt itself
                if (proteinID == endProteinID) {
                    continue;
                }
                if (point.start.containsKey(endProteinID)) {
                    Intron startInnerIntron = point.start.get(endProteinID);
                    int skiptExons = endInnerIntron.index - startInnerIntron.index;                                                
                    
                    if (skiptExons == 0) {
                        continue;
                    }

                    // Bingo !
                    long skiptedBases = endInnerIntron.exonSkippedBasesAcc - startInnerIntron.exonSkippedBasesAcc;

                    if (!skiptInfosMap.containsKey(svIntronRange)) {
                        SkipInfo si = createFromGene(gene, svIntronRange);
                        si.minSkippedExon = skiptExons;
                        si.minSkippedBases = skiptedBases;
                        skiptInfosMap.put(intron.location, si);    
                    }
                    
                    SkipInfo si = skiptInfosMap.get(svIntronRange);  
                    si.minSkippedExon = Math.min(skiptExons, si.minSkippedExon);
                    si.minSkippedBases = Math.min(skiptedBases, si.minSkippedBases);
                    si.maxSkippedExon = Math.max(skiptExons, si.maxSkippedExon);
                    si.maxSkippedBases = Math.max(skiptedBases, si.maxSkippedBases);
                    si.SVProps.add(proteinID);
                    si.WTProps.add(endInnerIntron.proteinID);
                    
                    Intron wtIntron = endInnerIntron;
                    while(wtIntron != null && startInnerIntron.location.start <= wtIntron.location.start) {
                        si.WTIntrons.add(wtIntron.location);
                        if (intronPoints.containsKey(wtIntron.location.start)) {
                            evaluatePoint(intronPoints.get(wtIntron.location.start), alreadyProcessed, skiptInfosMap, intronPoints, gene);
                        }
                        wtIntron = wtIntron.previous;
                    }
                }
            }
        }
    }

	private SkipInfo createFromGene(GeneReading gene,  Range svIntron) {
		SkipInfo si = new SkipInfo(gene.geneID);
        si.SVIntron = svIntron;
        si.geneID = gene.geneID;
        si.symbol = gene.geneName;
        si.chromosome = gene.sequenceName;
        si.strand = gene.ascending ? "+" : "-";
        si.numberProteins = gene.proteinCounter;
        si.numberTranscripts = gene.codingRegions.size();
		return si;
	}

    private class Node {
        Map<String, Intron> start = new HashMap<String, Intron>();
        Map<String, Intron> end = new HashMap<String, Intron>();
        long location;

        private Node(long location) {
            this.location = location;
        }
    }

    private class Intron {
        Range location;
        int index;
        String proteinID;
        Intron previous;
        long exonSkippedBasesAcc;
        long exonSkipped;
    }

    private Map<Long, Node> getIntronPoints(GeneReading gene){
        Map<Long, Node> introns = new HashMap<Long, Node>();
        
        for (Transcript sv : gene.codingRegions) {
            // It must have at least two CDS to be a SV
            if (sv.CDSs.size() < 2) {
                continue;
            }

            Intron previousIntron = null;
            for(int i = 0, j = 1; j < sv.CDSs.size(); i++, j++){
                int currentIndex = i;
                Range left = sv.CDSs.get(i);
                Range right = sv.CDSs.get(j);
                long leftExonLen = left.end - left.start + 1;
                Range intronLocation = new Range(left.end+1, right.start);
                Intron previousIn = previousIntron;
                
                long prevAcc = 0;
                if (previousIn != null) {
                    prevAcc += previousIn.exonSkippedBasesAcc;
                }
                long exonAcc = prevAcc + leftExonLen;
                
                Intron intron = new Intron(){{
                    location = intronLocation;
                    proteinID = sv.proteinID;
                    previous = previousIn;
                    index = currentIndex;
                    exonSkipped = leftExonLen;
                    exonSkippedBasesAcc = exonAcc;
                }};
                
                previousIntron = intron;

                //TODO: Add unit test for covering this functionality
                if (!introns.containsKey(intronLocation.start)) {
                    introns.put(intronLocation.start, new Node(intronLocation.start));
                }
                if (!introns.containsKey(intronLocation.end)) {
                    introns.put(intronLocation.end, new Node(intronLocation.end));
                }
                introns.get(intronLocation.start).start.put(sv.proteinID, intron);
                introns.get(intronLocation.end).end.put(sv.proteinID, intron);
            }            
        }

        return introns;        
    }
        
}

