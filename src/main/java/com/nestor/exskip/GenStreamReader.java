package com.nestor.exskip;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.nestor.exskip.model.GeneReading;
import com.nestor.exskip.model.Range;
import com.nestor.exskip.model.Transcript;

/**
 * GenGTFReader
 */
public class GenStreamReader {
    private static Logger log = Logger.getLogger("GenStreamReader");
    
    public Iterable<GeneReading> getGenes(String fileName) {
        
        return new Iterable<GeneReading>() {
            @Override
            public Iterator<GeneReading> iterator() {
                return new GeneIterator(fileName);
            }
        };
    }

    private class GeneIterator implements Iterator<GeneReading> {
        
        private String fileName;
        private GeneReading current;
        private GeneReading nextGene;
        private BufferedReader reader;
        
        public GeneIterator(String fileName) {
            super();
            this.fileName = fileName;
        }

        @Override
        public boolean hasNext() {                        
            
            if (this.reader == null) {
                this.reader = this.createBufferReader(fileName);
                this.nextGene = readToFirstGen(this.reader);
                
                // Not found in file
                if (this.nextGene == null) {
                    this. closeReader();
                    return false;
                }

            // There are no more in file
            } else if (this.nextGene == null){
                this.closeReader();
                return false;
            }

            GeneReading gr = this.nextGene;
            this.nextGene = null;
            
            Transcript tc = null;
            Set<String> proteinIDs = new HashSet<String>();
            String line;

            try {
                while((line = this.reader.readLine()) != null){
                String[] values = line.split("\t");
                if (values.length != 9) {
                    log.warning("Line with unexpected length:" + line);
                    continue;
                }

                long start = Long.parseLong(values[3]);
                long end = Long.parseLong(values[4]);

                Map<String, String> annotations = getAnnotation(values);
                if (annotations.containsKey("gene_id") && !annotations.get("gene_id").equals(gr.geneID)){
                    this.nextGene = getGene(values);
                    break;
                }
                
                if (annotations.containsKey("transcript_id") && (tc == null || !tc.id.equals(annotations.get("transcript_id")))) {
                    Transcript newTC = new Transcript();
                    gr.codingRegions.add(newTC);
                    newTC.id = annotations.get("transcript_id");
                    tc = newTC;
                }
                
                if (values[2].equals("CDS") && tc != null){
                    if(!annotations.containsKey("protein_id")) {
                        log.warning("protein_id annotation not found in " + values[8]);
                        continue;
                    }
                    String proteinID = annotations.get("protein_id");
                    proteinIDs.add(proteinID);
                    tc.proteinID = proteinID;
                    
                    // TODO: Encapsulate this logic inside the GeneReading
                    if (gr.ascending) {
                        tc.CDSs.add(new Range(start, end));                                                                    
                    } else {
                        tc.CDSs.add(0, new Range(start, end));                                
                    }
                }                            
            }
            } catch (Exception e) {
                log.warning("Error reading line" + e.getMessage());
                e.printStackTrace();
            }

            gr.proteinCounter = proteinIDs.size();
            this.current = gr;
            return true;
        }

        private BufferedReader createBufferReader(String fileName) {
            log.info("opening file " + fileName);
            File file = new File(fileName);
            InputStream in = null;
            try {
                in = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return new BufferedReader(new InputStreamReader(in));
        }

        private void closeReader() {
            try {
                this.reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public GeneReading next() {
            return this.current;
        }

        @Override
        public void remove() {
            
        }

        @Override
        public void finalize(){
            if(this.reader != null) {
                this.closeReader();
            }
        }

        private Map<String, String> getAnnotation(String[] values){
            String[] annotations = values[8].split(";");
            Map<String, String> annotMap = new HashMap<String, String>();
            for (String annot : annotations) {
                String[] kv = annot.trim().split(" ");
                annotMap.put(kv[0], kv[1].replace("\"", ""));
            }
            return annotMap;
        }

        private GeneReading readToFirstGen(BufferedReader r){
            String line;
            String[] values;
            try {
                while((line = r.readLine()) != null){
                    if(line.startsWith("#!")){
                        continue;
                    }
                    values = line.split("\t");
                    if (values.length != 9) {
                        log.warning("Line with unexpected length:" + line);
                        continue;
                    }
                    
                    if (getAnnotation(values).containsKey("gene_id")){
                        return getGene(values);
                    } 
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        private GeneReading getGene(String[] values) {
            String geneID = getAnnotation(values).get("gene_id");
            String geneName = getAnnotation(values).get("gene_name");
            String sequenceName = values[0];
            boolean ascending = true;
            if (values[6].equals("-")){
                ascending = false;
            }

            GeneReading gene = new GeneReading(geneID, ascending);
            gene.geneName = geneName;
            gene.sequenceName = sequenceName;
            gene.ascending = ascending;
            return gene;
        }
    }
}