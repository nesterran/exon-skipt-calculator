package com.nestor.exskip;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.nestor.exskip.model.Range;
import com.nestor.exskip.model.SkipInfo;

/**
 * ResultWriter
 */
public class ResultWriter {
    
    private Path path;
    private BufferedWriter writer;
    
    public ResultWriter(String filePath) {
        this.path = Paths.get(filePath);
        
        try {
            Files.deleteIfExists(this.path);
            this.writer = new BufferedWriter(new FileWriter(this.path.toString()), 500 * 1024 );            
		} catch (IOException e) {
			// TODO Auto-generated catch block
            e.printStackTrace();
            
		}
    }
    
    public Exception write(SkipInfo info) {
        try {
            this.writer.write(convertToString(info));
            this.writer.newLine();
        } catch (IOException e) {
           return e;
        }        
        return null;
    }

    public Exception writeHeader(){
        String[] headers = new String[]{"id	symbol","chr","strand","nprots","ntrans","SV","WT","WT_prots","SV_prots","min_skipped_exon","max_skipped_exon","min_skipped_bases","max_skipped_bases"};        
        String headerStr = String.join("\t", headers);
        try {
            this.writer.write(headerStr);
            this.writer.newLine();
		} catch (IOException e) {
			return e;
        }        
        return null;
    }

    private String convertToString(SkipInfo info){
        String[] values =  new String[]{
            info.geneID,
            info.symbol,
            info.chromosome,
            info.strand,
            "" + info.numberProteins,
            "" + info.numberTranscripts, 
            convertToStr(info.SVIntron),
            convertToStr(info.WTIntrons),
            String.join("|", info.WTProps),
            String.join("|", info.SVProps),
            // TODO
            "" + info.minSkippedExon,
            "" + info.maxSkippedExon,
            "" + info.minSkippedBases,
            "" + info.maxSkippedBases
        };
        
        return String.join("\t", values);        
    }

    private String convertToStr(Range r){
        return r.start + ":" + r.end;
    }

    private String convertToStr(Set<Range> rs){
        String[] elements = rs.stream().map(this::convertToStr).toArray(String[]::new);
        return String.join("|", elements);                    
    }

    public void close() {
        try {
	       this.writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}