package com.nestor.exskip;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.nestor.exskip.model.GeneReading;
import com.nestor.exskip.model.SkipInfo;

public class App 
{
    static Logger log = Logger.getLogger("exskip");
    public static void main( String[] args )
    {      
        try{
            log.setLevel(Level.ALL);
            Map<String, IExonSkipCalculator> calculators = new HashMap<String, IExonSkipCalculator>();
            calculators.put("2", new ExonSkipCalculator());
            calculators.put("1", new ExonSkipCalculatorV1());

            Map<String, String> params = new HashMap<String, String>();
            params.put("version", "2");
            params.put("parallel", "1");
            String key = "";
            for (String arg : args) {
                if (key == "") {
                    if (arg.equals("-gtf")){
                        key = "input";
                    } else if (arg.equals("-o")){
                        key = "output";
                    } else if (arg.equals("-v")) {
                        key = "version";
                    } else if (arg.equals("-p")) {
                        key = "parallel";
                    }
                } else {
                    params.put(key, arg);
                    key = "";
                }            
            }

            if (params.size() < 2) {
                System.out.println("invalid parameters. Expected -gtf and -o");
                System.exit(1);
            }

            boolean parallel = params.get("parallel").equals("1");
            String inputFilePath = params.get("input");
            String outputFilePath = params.get("output");            
            IExonSkipCalculator calculator = calculators.get(params.get("version"));
            Instant start = Instant.now();
            
            log.info("[START] at " + start + " \nFrom " + inputFilePath + "\nTo " + outputFilePath + "\n Version " + params.get("version") + "\nParallel " + parallel);
            calculate(params.get("version"), inputFilePath, outputFilePath, parallel);
            Instant end = Instant.now();

            log.info("[Finish] time " + Duration.between(start, end) + " \nFrom " + inputFilePath + "\nTo " + outputFilePath + "\n Version " + params.get("version") + "\nParallel " + parallel);            
        } catch(Exception e) {
            log.warning("ERROR " + e.getMessage());
            e.printStackTrace();
        }
    }
	private static void calculate(String version , String inputFilePath, String outputFilePath, boolean parallel) {
		GenStreamReader grStream = new GenStreamReader();
        ResultWriter wt = new ResultWriter(outputFilePath);
        Exception e = wt.writeHeader();
        if (e != null) {
            log.warning("failed to write header");
            System.exit(1);
        }
        if (parallel) {
            Iterator<GeneReading> sourceIterator = grStream.getGenes(inputFilePath).iterator();
            Iterable<GeneReading> iterable = () -> sourceIterator;
            Stream<GeneReading> geneStream = StreamSupport.stream(iterable.spliterator(), parallel);
            geneStream.parallel().forEach(gr -> {  
                exec(version, wt, gr);
            });
        } else{
            for (GeneReading gr : grStream.getGenes(inputFilePath)) {
                exec(version, wt, gr);
            }
        }

        wt.close();
    }
    
	private static void exec(String version, ResultWriter wt, GeneReading gr) {
		IExonSkipCalculator calc = version.equals("2") ? new ExonSkipCalculator() : new ExonSkipCalculatorV1();
            SkipInfo[] result = calc.Calculate(gr);
            for (SkipInfo si : result) {
                Exception ex = wt.write(si);
                if (ex != null) {
                    log.warning("failed to write SV Intron");
                    System.exit(1);
                }
            }
	}
}
