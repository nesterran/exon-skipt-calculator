package com.nestor.exskip;

import com.nestor.exskip.model.GeneReading;
import com.nestor.exskip.model.SkipInfo;

public interface IExonSkipCalculator {
    SkipInfo[] Calculate(GeneReading gene);
}
