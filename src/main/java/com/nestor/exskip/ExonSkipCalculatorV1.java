package com.nestor.exskip;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.nestor.exskip.model.*;
/**
 * ExonSkipCalculator
 */
public class ExonSkipCalculatorV1 implements IExonSkipCalculator {
    
    public ExonSkipCalculatorV1() {
        super();
    }

    public SkipInfo[] Calculate(GeneReading gene)  {
        Map<Range, Set<String>> svIntronCandidates = getIntrons(gene);
        Map<Range, SkipInfo> skiptInfosMap = new HashMap<Range,SkipInfo>();

        for (Transcript wt : gene.codingRegions) {
            // TODO: This can be optimized to not iterate N times the wt's CDSs but only once
            // but it makes things more complex :(
            for (Map.Entry<Range, Set<String>> kv : svIntronCandidates.entrySet()) {
                List<Range> cdsCandidates = new ArrayList<Range>();
                Range cdsLeft = null;
                Range cdsRight = null;
                Range svIntron = kv.getKey();

                for (Range cds : wt.CDSs) {
                    
                    if (cds.end + 1 == svIntron.start) {
                        cdsLeft = cds;
                    }
                    
                    if (cds.start == svIntron.end) {
                        cdsRight = cds;
                        break;
                    }

                    // Add candidate
                    if(cdsLeft != null && cds.start > svIntron.start && cds.end < svIntron.end){
                        cdsCandidates.add(cds);
                    }
                }

                // Bingo !
                if (cdsLeft != null && cdsRight != null && cdsCandidates.size() > 0) {
                    
                    int skiptExons = cdsCandidates.size();
                    long skiptedBases = cdsCandidates
                        .stream()
                        .mapToLong(cds -> cds.end - cds.start + 1)
                        .sum();

                    if(!skiptInfosMap.containsKey(svIntron)){
                        SkipInfo si = createFromGene(gene, kv.getValue(), svIntron);
                        si.minSkippedExon = skiptExons;
                        si.minSkippedBases = skiptedBases;
                        skiptInfosMap.put(svIntron, si);                            
                    }
                    
                    SkipInfo si = skiptInfosMap.get(svIntron);                        
                    si.minSkippedExon = Math.min(skiptExons, si.minSkippedExon);
                    si.minSkippedBases = Math.min(skiptedBases, si.minSkippedBases);
                    si.maxSkippedExon = Math.max(skiptExons, si.maxSkippedExon);
                    si.maxSkippedBases = Math.max(skiptedBases, si.maxSkippedBases);
                    si.WTProps.add(wt.proteinID);
                    
                    Range last = cdsLeft;
                    for (int i=0; i < cdsCandidates.size(); i++) {                        
                        Range cds = cdsCandidates.get(i);
                        Range left = i == 0 ? cdsLeft : cdsCandidates.get(i-1);                        
                        si.WTIntrons.add(new Range(left.end +1, cds.start));                        
                        last = cds;
                    }                    
                    
                    si.WTIntrons.add(new Range(last.end + 1, cdsRight.start));
                }
            }
        }

        int size = skiptInfosMap.values().size();
        return skiptInfosMap.values().toArray(new SkipInfo[size]);
    }

	private SkipInfo createFromGene(GeneReading gene, Set<String> svProps, Range svIntron) {
		SkipInfo si = new SkipInfo(gene.geneID);
        si.SVIntron = svIntron;
        si.SVProps = svProps;
        si.geneID = gene.geneID;
        si.symbol = gene.geneName;
        si.chromosome = gene.sequenceName;
        si.strand = gene.ascending ? "+" : "-";
        si.numberProteins = gene.proteinCounter;
        si.numberTranscripts = gene.codingRegions.size();
		return si;
	}

    private Map<Range, Set<String>> getIntrons(GeneReading gene){
        // TODO: Convert LIST<Range> in a Map with a Set to avoid duplicates
        // and store the sv.ID
        Map<Range, Set<String>> introns = new HashMap<Range, Set<String>>();
        
        for (Transcript sv : gene.codingRegions) {
            // It must have at least two CDS to be a SV
            if (sv.CDSs.size() < 2) {
                continue;
            }

            for(int i = 0, j = 1; j < sv.CDSs.size(); i++, j++){
                Range left = sv.CDSs.get(i);
                Range right = sv.CDSs.get(j);
                Range svIntron = new Range(left.end+1, right.start);
                if (!introns.containsKey(svIntron)){
                    introns.put(svIntron, new HashSet<String>());
                }
                introns.get(svIntron).add(sv.proteinID);
            }            
        }

        return introns;        
    }

        
}

