package com.nestor.exskip.model;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GeneReading {
    public String geneID;
    public boolean ascending;
    public String geneName;
    public String sequenceName;
    public int proteinCounter;
    public List<Transcript> codingRegions = new ArrayList<Transcript>();

    public GeneReading() {        
    }

    public GeneReading(String geneID, boolean ascending) {
        this.geneID = geneID;
        this.ascending = ascending;
    }
}
