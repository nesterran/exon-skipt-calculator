package com.nestor.exskip.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Transcript {
    public String proteinID;
    public List<Range> CDSs;
    public String id;

    public Transcript() {
        this.CDSs = new ArrayList<Range>();
    }

    public Transcript(String proteinID, List<Range> CDSs) {
        this.proteinID = proteinID;
        this.CDSs = CDSs;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.proteinID, this.CDSs);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Transcript)){
            return false;
        }

        Transcript r = (Transcript)obj;
        return r.id == this.id && r.proteinID == this.proteinID && this.CDSs.containsAll(r.CDSs) && r.CDSs.containsAll(this.CDSs) ;

    }

}

