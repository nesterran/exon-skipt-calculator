package com.nestor.exskip.model;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class SkipInfo {
    public String geneID;
    public String symbol;
    public String chromosome;
    public String strand;
    public int numberProteins;
    public int numberTranscripts;
    public Range SVIntron;
    public List<String> WTProps;
    public Set<String> SVProps;
    public Set<Range> WTIntrons;
    public long minSkippedExon;
    public long maxSkippedExon;
    public long minSkippedBases;
    public long maxSkippedBases;

    public SkipInfo() {
        this.WTIntrons = new HashSet<Range>();
        this.WTProps = new ArrayList<String>();
        this.SVProps = new HashSet<String>();
        this.SVIntron = new Range();
    }

    public SkipInfo(String geneID) {
        this();
        this.geneID = geneID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.geneID, this.symbol, this.chromosome, this.strand, this.numberProteins, 
        this.numberTranscripts, this.WTIntrons, this.SVIntron, this.strand, this.symbol);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SkipInfo)){
            return false;
        }

        SkipInfo r = (SkipInfo)obj;
        
        return this.chromosome == r.chromosome &&
            this.geneID == r.geneID &&
            this.maxSkippedBases == r.maxSkippedBases &&
            
            this.maxSkippedExon == r.maxSkippedExon &&
            this.minSkippedBases == r.minSkippedBases &&
            this.minSkippedExon == r.minSkippedExon &&
            this.numberProteins == r.numberProteins &&
            this.numberTranscripts == r.numberTranscripts &&
            this.strand == r.strand &&
            this.symbol == r.symbol &&
            this.SVIntron.equals(r.SVIntron) &&
            
            this.WTIntrons.containsAll(r.WTIntrons) && 
            r.WTIntrons.containsAll(this.WTIntrons) &&
            this.WTIntrons.size() == r.WTIntrons.size() &&
            
            this.WTProps.containsAll(r.WTProps) && 
            r.WTProps.containsAll(this.WTProps) &&
            this.WTProps.size() == r.WTProps.size() &&
            
            this.SVProps.containsAll(r.SVProps) && 
            r.SVProps.containsAll(this.SVProps) &&
            this.SVProps.size() == r.SVProps.size();
    }
}
