package com.nestor.exskip.model;

import java.util.Objects;

public class Range {
    public long start;
    public long end;

    public Range() {        
    }

    public Range(long start, long end) {
        this.start = start;
        this.end = end;        
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.start, this.end);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Range)){
            return false;
        }

        Range r = (Range)obj;
        return r.start == this.start && r.end == this.end;

    }

}

